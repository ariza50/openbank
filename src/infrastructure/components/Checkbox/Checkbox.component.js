import React, {Component} from 'react';
import './Checkbox.component.scss';

export class Checkbox extends Component {

  render() {
    return (
      <div className="checkbox-component">
        <input
          id={this.props.label}
          name="remember-me"
          type="checkbox"
          checked={this.props.value}
          onChange={(e) => this.props.onChange(e.target.checked)}
          className="checkbox"
        />
        <label htmlFor={this.props.label} className="label">
          {this.props.label}
        </label>
      </div>
    );
  }
}

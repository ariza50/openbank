import React, {Component} from 'react';
import {ModalComponent} from '../../../../infrastructure/components/Modal/Modal.component';
import {StepsHeader} from '../../../../infrastructure/components/StepsHeader/StepsHeader.component';
import {Feedback} from './partials/feedback';
import {ProductForm} from './partials/ProductForm';
import {ProductInformation} from './partials/ProductInformation';
import {PasswordManagerWizardResource} from './resources/PasswordManagerWizard.resource';

export class PasswordManagerWizard extends Component {

  constructor(props) {
    super(props);

    this.steps = [
      {name: 'ProductInformation'},
      {name: 'Form'},
      {name: 'Feedback'},
    ];

    this.state = {
      selectedStep: 0,
      productForm: {
        ageTerms: false,
        privacyPolicyTerms: false,
      },
      productInformationForm: {
        passwordReminder: '',
        password: '',
        repeatPassword: '',
      },
      callStatus: undefined,
    };
  }

  componentDidMount() {
    this.setState({selectedStep: 0});
  }

  render() {
    return (
      <ModalComponent
        labelButtonLeft="Cancelar"
        labelButtonRight="Siguiente"
        open={this.props.open}
        onClose={() => this.closeWizard()}
      >
        {
          this.state.selectedStep !== 2 ?
            <StepsHeader
              steps={this.steps}
              selectedStep={this.state.selectedStep}
            /> :
            null
        }
        {this.renderStep()}
      </ModalComponent>
    );
  }

  renderStep() {
    switch (this.state.selectedStep) {
      case 0:
        return (
          <ProductInformation
            onClose={() => this.closeWizard()}
            onNextStep={(productForm) => this.submitStep('productForm', productForm)}
          />
        );
      case 1:
        return (
          <ProductForm
            form={this.state.productInformationForm}
            onClose={() => this.closeWizard()}
            onNextStep={(productInformationForm) => this.submitStep('productInformationForm', productInformationForm)}
          />);
      case 2:
        return (
          <Feedback
            callStatus={this.state.callStatus}
            onClose={() => this.closeWizard()}
          />
        );
      default:
        return (<ProductInformation />);
    }
  }

  submitStep(form, value) {
    this.setState({[form]: value});

    if (this.state.selectedStep === 1) {
      this.createNewMasterPassword();
    }
    this.setState({selectedStep: this.state.selectedStep + 1});
  }

  closeWizard() {
    this.setState({
      selectedStep: 0,
      productForm: {
      ageTerms: false,
        privacyPolicyTerms: false,
    },
    productInformationForm: {
      passwordReminder: '',
        password: '',
        repeatPassword: '',
    },
    callStatus: undefined,
    });
    this.props.onClose();
  }

  createNewMasterPassword() {
    //  Change request to 'pruebaKO123' to get error response.
    PasswordManagerWizardResource.create('pruebaKO12').then(response => {
      console.log('Status', response.status);
      this.setState({callStatus: response.status});
    }).catch(error => {
      console.log('Status', error.status);
      this.setState({callStatus: error.status});
    });
  }
}

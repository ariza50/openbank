import React, {Component} from 'react';
import {ChevronRightIcon} from '@heroicons/react/solid';
import './Button.component.scss';

export class Button extends Component {
  render() {
    return (
      <button
        onClick={this.props.onClick}
        disabled={this.props.disabled}
        type="button"
        className={this.getClassNames()}
      >
        {this.props.children}
        {
          this.props.chevronRight ?
            <ChevronRightIcon className="chevron" /> :
            null
        }
      </button>
    );
  }

  getClassNames() {
    let classNames = ['btn-component', this.props.type];
    if (this.props.round) {
      classNames.push('rounded-full');
    }
    if (this.props.full) {
      classNames.push('w-full');
    }
    return classNames.join(' ');
  }
}

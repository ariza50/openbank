import {EyeIcon, EyeOffIcon, InformationCircleIcon} from '@heroicons/react/outline';
import React, {Component} from 'react';
import './Input.component.scss';
import translate from '../../../locale/Translate';

export class Input extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showPassword: false,
      type: this.props.type,
      canValidate: false,
    };
  }

  render() {
    const size = this.props.large ? 'large': '';
    return (
      <div className={`input-component ${size}`}>
        <div className="flex justify-left">
          {this.renderLabel()}
          {
            this.props.renderInfo ?
              this.renderInfoIcon() :
              null
          }
        </div>
        <div className={`mt-2 ${this.props.type === 'password' ? 'relative rounded-md shadow-sm' : ''}`}>
          <input
            type={this.state.type}
            name="input"
            value={this.props.value}
            className={this.getInputClassType()}
            placeholder={this.props.placeholder}
            aria-describedby="input-optional"
            onChange={(e) => this.props.onChange(e.target.value)}
            onBlur={() => {
              this.setState({canValidate: true}, () => this.emitValidation(this.props.value));
            }}
          />
          {this.props.type === 'password' ? this.renderPasswordEyeIcon() : null}
        </div>
        {this.renderErrorDescription()}
        {this.props.showCounter ? this.renderWordCounter() : null}
      </div>
    );
  }

  renderInfoIcon() {
    return <span className="exclamation-icon" id="input-optional">
                <InformationCircleIcon className="w-5 h-5 text-blue-300" />
              </span>;
  }

  renderLabel() {
    return <label htmlFor="email" className="label">
      {this.props.label}
    </label>;
  }

  getInputClassType() {
    if (!this.isValid(this.props.value)) {
      return ('input-error');
    }
    return ('input');
  }

  renderErrorDescription() {

    if (!this.isValid(this.props.value)) {
      let errorDescription = translate('general.input.password');

      if (!!this.props.customErrorDescription && this.props.customErrorDescription !== '') {
        errorDescription = this.props.customErrorDescription;
      }

      return (
        <p className="error-description" id="email-error">
          {errorDescription}
        </p>);
    }
  }

  renderWordCounter() {
    return <div className="word-counter">
      {this.getInputCount()}
    </div>;
  }

  renderPasswordEyeIcon() {
    return this.state.type === 'password' ?
      <div
        className="icon"
        onClick={() => this.showPassword()}
      >
        <EyeIcon className={`eye ${this.isValid(this.props.value) ? '' : 'error'}`} aria-hidden="true" />
      </div> :
      <div
        className="icon"
        onClick={() => this.showPassword()}
      >
        <EyeOffIcon className={`eye ${this.isValid(this.props.value) ? '' : 'error'}`} aria-hidden="true" />
      </div>;
  }

  getInputCount() {
    if (!this.props.value) {
      return '0/255';
    }
    return (`${this.props.value.length}/255`);
  }

  showPassword() {
    if (this.state.showPassword) {
      this.setState({showPassword: false, type: 'password'});
    } else {
      this.setState({showPassword: true, type: 'text'});
    }
  }

  isValid(value) {
    let validation = false;

    if (!this.props.required) {
      return true;
    }

    if (this.props.valid === false) {
      return false;
    }

    if (!this.state.canValidate) {
      return true;
    }

    if (!value && this.props.required) {
      return false;
    }

    if (!value) {
      return true;
    }

    validation = this.validatePassword(value);

    return validation;
  }

  emitValidation(value) {
    if (!!this.props.validation) {
      const isValid = this.isValid(value);
      this.props.validation(isValid);

    }
  }

  validatePassword(password: string) {
    return !!(password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,24}$/));
  }
}

import React, {Component} from 'react';

export class SkeletonFooter extends Component {

  render() {
    return (
      <div className="border-t-2 border-gray-100">
        <div className="px-4 my-5 sm:px-6 sm:flex sm:flex-row sm:justify-between sm:my-4 sm:mx-10">
          <div />
          <div className="w-40 h-6 bg-gray-400 rounded animate-pulse" />
        </div>
      </div>
    );
  }
}

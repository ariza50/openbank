import React, {Component} from 'react';
import vaultIcon from '../../../../../assets/img/group-3.svg';
import passwordsIcon from '../../../../../assets/img/group.svg';
import {Button} from '../../../../../infrastructure/components/Button/Button.component';
import {Card} from '../../../../../infrastructure/components/Card/Card.component';
import {Checkbox} from '../../../../../infrastructure/components/Checkbox/Checkbox.component';
import {FormCardGroup} from '../../../../../infrastructure/components/Form/FormCardGroup.component';
import {FormModalContent} from '../../../../../infrastructure/components/Form/FormModal.component';
import {FormModalFooter} from '../../../../../infrastructure/components/Form/FormModalFooter';
import {MainTitle} from '../../../../../infrastructure/components/MainTitle/MainTitle.component';
import {Paragraph} from '../../../../../infrastructure/components/Paragraph/Paragraph.component';
import translate from '../../../../../locale/Translate';

export class ProductInformation extends Component {

  constructor(props) {
    super(props);

    this.state = {
      form: {
        ageTerms: false,
        privacyPolicyTerms: false,
      },
      disableNextStep: true,
    };
  }

  render() {
    return (
      <>
        <FormModalContent>
          <MainTitle>
            {translate('productInformation.title')}
          </MainTitle>
          <FormCardGroup>
            <Card
              icon={passwordsIcon}
            >
              {translate('productInformation.card1Description')}
            </Card>
            <Card
              icon={vaultIcon}
            >
              {translate('productInformation.card2Description')}
            </Card>
          </FormCardGroup>
          <Paragraph
            title={translate('productInformation.paragraph1.title')}
            description={translate('productInformation.paragraph1.description')}
          />
          <Paragraph
            title={translate('productInformation.paragraph2.title')}
            description={translate('productInformation.paragraph2.description')}
          />
          <div className="mt-6">
            <Checkbox
              label={translate('productInformation.ageTerms')}
              value={this.state.ageTerms}
              onChange={(value) => this.updateValidations('ageTerms', value)}
            />
            <Checkbox
              label={translate('productInformation.privacyPolicyTerms')}
              value={this.state.privacyPolicyTerms}
              onChange={(value) => this.updateValidations('privacyPolicyTerms', value)}
            />
          </div>
        </FormModalContent>
        <FormModalFooter>
          {this.renderMenuButtons()}
        </FormModalFooter>
      </>
    );
  }

  renderMenuButtons() {
    return (
      <>
        <Button
          type="secondary"
          onClick={() => this.props.onClose()}
        >
          {translate('productInformation.cancel')}
        </Button>
        <Button
          type="primary"
          onClick={() => this.props.onNextStep(this.state.form)}
          chevronRight={true}
          disabled={this.state.disableNextStep}
        >
          {translate('productInformation.next')}
        </Button>
      </>
    );
  }

  updateValidations(field, value) {
    let form = {...this.state.form};
    form = {
      ...form,
      [field]: value,
    };
    this.setState({form}, () => this.checkFormValid());
  }

  checkFormValid() {
    for (let field in this.state.form) {
      if (!this.state.form.hasOwnProperty(field)) {
        this.setState({disableNextStep: true});
        return;
      }
      if (!this.state.form[field]) {
        this.setState({disableNextStep: true});
        return;
      }
    }
    this.setState({disableNextStep: false});
  }
}

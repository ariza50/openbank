import React, {Component} from 'react';

export class FormModalContent extends Component {

  render() {
    return (
      <div className="flex flex-col justify-start px-16 py-10">
        {this.props.children}
      </div>
    );
  }
}

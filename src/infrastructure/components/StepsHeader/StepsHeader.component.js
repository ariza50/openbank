import React, {Component} from 'react';
import {CheckIcon} from '@heroicons/react/solid';
import {ChevronUpIcon} from '@heroicons/react/solid';
import './StepsHeader.component.scss';

export class StepsHeader extends Component {

  render() {
    return (
      <nav aria-label="Progress" className="steps-component">
        <ol className="steps-content">
          {this.props.steps.map((step, stepIdx) => (
            <li
              key={step.name}
              className={this.getStepClassName(stepIdx)}>
              {this.renderStep(step, stepIdx)}
            </li>
          ))}
        </ol>
      </nav>
    );
  }

  renderStep(step, stepIdx) {
    return <>
      {stepIdx < this.props.selectedStep ? this.renderStepComplete() : stepIdx === this.props.selectedStep ? this.renderCurrentStep(stepIdx) : this.renderUpcomingStep(stepIdx)}
    </>;
  }

  renderUpcomingStep(stepIdx) {
    return <>
      <div className="step-line" aria-hidden="true">
        <div className="line" />
      </div>
      <div
        className="step-upcoming"
      >
        <span className="text-white">{stepIdx + 1}</span>
      </div>
    </>;
  }

  renderCurrentStep(stepIdx) {
    return <>
      <div className="step-line" aria-hidden="true">
        <div className="line" />
      </div>
      <div
        className="step-current"
        aria-current="step"
      >
        <span className="text-white">{stepIdx + 1}</span>
      </div>
      <div
        className="chevron-up"
      >
        <ChevronUpIcon className="w-24 h-24 text-white" aria-hidden="true" />
      </div>
    </>;
  }

  renderStepComplete() {
    return <>
      <div className="step-line" aria-hidden="true">
        <div className="line complete" />
      </div>
      <div
        className="step-complete"
      >
        <CheckIcon className="w-5 h-5 text-white" aria-hidden="true" />
      </div>
    </>;
  }

  static classNames(...classes) {
    return classes.filter(Boolean).join(' ');
  }

  getStepClassName(stepIdx: number) {
    return StepsHeader.classNames(stepIdx !== this.props.steps.length - 1 ? 'pr-8 sm:pr-8' : '', 'relative');
  }
}

import React, {Component} from 'react';
import './Card.component.scss';

export class Card extends Component {

  render() {
    return (
      <div className="card-component">
        <img className="card-image" alt="icon" src={this.props.icon} />
        <p className="card-description">
          {this.props.children}
        </p>
      </div>
    );
  }
}

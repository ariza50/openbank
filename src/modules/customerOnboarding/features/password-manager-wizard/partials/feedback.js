import React, {Component} from 'react';
import success from '../../../../../assets/img/success.svg';
import warning from '../../../../../assets/img/warning.svg';
import {Button} from '../../../../../infrastructure/components/Button/Button.component';
import {FeedbackDescription} from '../../../../../infrastructure/components/FeedbackDescription/FeedbackDescription.component';
import {FormModalFooter} from '../../../../../infrastructure/components/Form/FormModalFooter';
import {SkeletonFeedbackDescription} from '../../../../../infrastructure/components/Skeleton/SkeletonFeedbackDescription';
import {SkeletonFooter} from '../../../../../infrastructure/components/Skeleton/SkeletonFooter';
import translate from '../../../../../locale/Translate';

export class Feedback extends Component {

  render() {
    if (!this.props.callStatus) {
      return this.renderSavingFeedback();
    }
    return (this.props.callStatus === 200 ? this.renderSuccessFeedback() : this.renderErrorFeedback());
  }

  renderSuccessFeedback() {
    return (
      <>
        <FeedbackDescription
          title={translate('feedbackForm.success.title')}
          description={translate('feedbackForm.success.description')}
          icon={success}
        />
        <FormModalFooter>
          <div />
          <Button
            type="link"
            onClick={() => this.props.onClose()}
            chevronRight={true}
          >
            {translate('feedbackForm.success.access')}
          </Button>
        </FormModalFooter>
      </>
    );
  }

  renderErrorFeedback() {
    return (
      <>
        <FeedbackDescription
          title={translate('feedbackForm.error.title')}
          description={translate('feedbackForm.error.description')}
          icon={warning}
        />
        <FormModalFooter>
          <div />
          <Button
            type="link"
            onClick={() => this.props.onClose()}
            chevronRight={true}
          >
            {translate('feedbackForm.error.access')}
          </Button>
        </FormModalFooter>
      </>
    );
  }

  renderSavingFeedback() {
    return (
      <>
        <SkeletonFeedbackDescription />
        <SkeletonFooter />
      </>
    );
  }
}

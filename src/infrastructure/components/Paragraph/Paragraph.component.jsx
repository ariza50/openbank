import React, {Component} from 'react';
import './Paragraph.component.scss';

export class Paragraph extends Component {

  render() {
    return (
      <div className="paragraph-component">
        <div className="title">
          {this.props.title}
        </div>
        <div className="description">
          {this.props.description}
        </div>
      </div>
    );
  }
}

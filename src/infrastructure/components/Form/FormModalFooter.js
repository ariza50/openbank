import React, {Component} from 'react';

export class FormModalFooter extends Component {

  render() {
    return (
      <div className="border-t-2 border-gray-100">
        <div className="px-4 my-5 sm:px-6 sm:flex sm:flex-row sm:justify-between sm:my-4 sm:mx-10">
          {this.props.children}
        </div>
      </div>
    );
  }
}

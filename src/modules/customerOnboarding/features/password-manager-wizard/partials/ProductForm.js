import React, {Component} from 'react';
import {Button} from '../../../../../infrastructure/components/Button/Button.component';
import {FormModalContent} from '../../../../../infrastructure/components/Form/FormModal.component';
import {FormModalFooter} from '../../../../../infrastructure/components/Form/FormModalFooter';
import {Input} from '../../../../../infrastructure/components/Input/Input.component';
import {MainTitle} from '../../../../../infrastructure/components/MainTitle/MainTitle.component';
import {Paragraph} from '../../../../../infrastructure/components/Paragraph/Paragraph.component';
import translate from '../../../../../locale/Translate';
import {ProductFormService} from './ProductForm.service';

export class ProductForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      form: {
        ...props.form,
        validations: {
          isValid: {
            password: true,
            repeatPassword: true,
          },
          isValidBusiness: {
            password: true,
            repeatPassword: true,
          },
          errorValidations: {
            password: '',
            repeatPassword: '',
          },
        },
      },
      disableNextStep: true,
    };
  }

  render() {
    if (this.props.form) {
      return (
        <>
          <FormModalContent>
            <MainTitle>
              {translate('productForm.title')}
            </MainTitle>
            <Paragraph
              description={translate('productForm.description1')}
            />
            <div className="flex justify-start space-x-6">
              <Input
                label={translate('productForm.passwordInput.label')}
                type="password"
                placeholder={translate('productForm.passwordInput.placeholder')}
                valid={this.state.form.validations.isValidBusiness.password}
                required={true}
                value={this.state.form.password}
                customErrorDescription={this.state.form.validations.errorValidations.password}
                onChange={(value) => this.onInputChange('password', value)}
                validation={(isValid) => this.updateValidationField('password', isValid)}
              />
              <Input
                label={translate('productForm.passwordRepeatInput.label')}
                type="password"
                valid={this.state.form.validations.isValidBusiness.repeatPassword}
                required={true}
                placeholder={translate('productForm.passwordRepeatInput.placeholder')}
                value={this.state.form.repeatPassword}
                customErrorDescription={this.state.form.validations.errorValidations.repeatPassword}
                onChange={(value) => this.onInputChange('repeatPassword', value)}
                validation={(isValid) => this.updateValidationField('repeatPassword', isValid)}
              />
            </div>
            <Paragraph
              description={translate('productForm.description2')}
            />
            <Input
              label={translate('productForm.passwordReminderInput.label')}
              type="text"
              placeholder={translate('productForm.passwordReminderInput.placeholder')}
              renderInfo={true}
              required={false}
              large={true}
              value={this.state.form.passwordReminder}
              showCounter={true}
              onChange={(value) => this.onInputChange('passwordReminder', value)}
            />
          </FormModalContent>
          <FormModalFooter>
            {this.renderMenuButtons()}
          </FormModalFooter>
        </>
      );
    }
    return null;
  }

  renderMenuButtons() {
    return <>
      <Button
        type="secondary"
        onClick={() => this.props.onClose()}
      >
        {translate('productForm.cancel')}
      </Button>
      <Button
        type="primary"
        onClick={() => this.validateNextStep()}
        chevronRight={true}
        disabled={this.state.disableNextStep}
      >
        {translate('productForm.next')}
      </Button>
    </>;
  }

  onInputChange(field, value) {
    let form = {...this.state.form};
    form = {
      ...form,
      [field]: value,
    };
    form.validations.isValidBusiness = {
      ...form.validations.isValidBusiness,
      [field]: true,
    };
    form.validations.errorValidations = {
      ...form.validations.errorValidations,
      [field]: '',
    };
    this.setState({form});
  }

  updateValidationField(field, value) {
    let form = {...this.state.form};
    form.validations.isValid = {
      ...form.validations.isValid,
      [field]: value,
    };
    this.checkValidForm()
    this.setState({form}, () => this.checkValidForm());
  }

  checkValidForm() {
    for (let validation in this.state.form.validations.isValid) {
      if (!this.state.form.validations.isValid.hasOwnProperty(validation)) {
        this.setState({disableNextStep: true});
        return false;
      }
      if (!this.state.form.validations.isValid[validation] || this.state.form[validation] === '') {
        this.setState({disableNextStep: true});
        return false;
      }
    }
    this.setState({disableNextStep: false});
    return true;
  }

  validateNextStep() {

    if (!this.checkValidForm()) {
      return;
    }

    const service = new ProductFormService(this.state.form);
    this.setState({form: service.businessLogicValidations()});

    if (!this.checkBusinessValidations()) {
      return;
    }

    let form = {...this.state.form,}
    delete form.validations;

    this.props.onNextStep(form);
  }

  checkBusinessValidations() {
    for (let validation in this.state.form.validations.isValidBusiness) {
      if (!this.state.form.validations.isValidBusiness.hasOwnProperty(validation)) {
        this.setState({disableNextStep: true});
        return false;
      }
      if (!this.state.form.validations.isValidBusiness[validation]) {
        this.setState({disableNextStep: true});
        return false;
      }
    }
    this.setState({disableNextStep: false});

    return true;
  }
}

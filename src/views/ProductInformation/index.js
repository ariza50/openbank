import React, {Component} from "react";

import step from './step1.png';

class Step1 extends Component {
    render() {
        return <img src={step} alt="Step1"/>
    }
}

export default Step1;

import {Component} from 'react';
import {submitForm} from '../../../../../services/api';

class PasswordManagerWizardResourceSingleton extends Component {

  static route = '/api/v1/create-master-password';

  constructor() {
    super(PasswordManagerWizardResourceSingleton.route);
  }

  static _instance: PasswordManagerWizardResourceSingleton;

  static get instance() {
    return this._instance || (this._instance = new this());
  }

  create(request) {
    return submitForm(request);
  }
}

export const PasswordManagerWizardResource = PasswordManagerWizardResourceSingleton.instance;

import {Component} from 'react';
import translate from '../../../../../locale/Translate';

export class ProductFormService extends Component {

  constructor(form) {
    super();

    this.form = form;
  }

  businessLogicValidations() {
    this.validateSamePassword();

    return this.form;
  }

  validateSamePassword() {
    if (this.form.password !== this.form.repeatPassword) {
      this.form.validations.errorValidations.repeatPassword = translate('general.input.repeatedPassword');
      this.form.validations.isValidBusiness.repeatPassword = false;
    }
  }
}

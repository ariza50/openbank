import React, {Component} from 'react';
import './MainTitle.component.scss';

export class MainTitle extends Component {

  render() {
    const align = this.props.align || 'left';
    return (
      <div className={`main-title-component ${align}`}>
        <div className="title-text">
          {this.props.children}
        </div>
        {this.props.withoutLine ? '' :  <div className="line" />}
      </div>
    );
  }
}

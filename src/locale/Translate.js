import I18n from 'i18n-js';
import en from './en';
import es from './es';

I18n.fallbacks = true;
I18n.missingBehaviour = 'guess';
I18n.defaultLocale = 'es';
I18n.locale = 'es';
I18n.translations = {
  es,
  en,
};
export const setLocale = (locale: string) => {
  switch (locale) {
    case 'es':
      locale = 'es';
      break;
    case 'en':
      locale = 'en';
      break;
    default:
      locale = 'en';
      break;
  }
  I18n.locale = locale;
  I18n.defaultLocale = locale;
};
export const getCurrentLocale = () => I18n.locale;
const translate = I18n.translate.bind(I18n);

export default translate;

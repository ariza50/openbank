import React, {Component} from 'react';

export class SkeletonFeedbackDescription extends Component {

  render() {
    return (
      <div className="flex justify-start px-16 py-10">
        <div className=" h-14 w-14 mr-8 bg-gray-400 rounded-full animate-pulse" />
        <div className="flex flex-col justify-center items-start">
          <div className="w-72 h-10 bg-gray-400 rounded animate-pulse"/>
          <div className="w-80 h-6 mt-4 bg-gray-400 rounded animate-pulse"/>
        </div>
      </div>
    );
  }
}

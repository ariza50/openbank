import React, {Component} from 'react';

export class FormCardGroup extends Component {

  render() {
    return (
      <div className="flex justify-around px-20">
        {this.props.children}
      </div>
    );
  }
}

import React, {Component} from 'react';
import './FeedbackDescription.component.scss';

export class FeedbackDescription extends Component {

  render() {
    return (
      <div className="feedback-description-component">
        <img className="icon" alt="icon" src={this.props.icon} />
        <div className="feedback-description-cols">
          <div className="title">
            {this.props.title}
          </div>
          <div className="description">
            {this.props.description}
          </div>
        </div>
      </div>
    );
  }
}

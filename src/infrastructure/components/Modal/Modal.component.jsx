import {Dialog, Transition} from '@headlessui/react';
import React, {Component, createRef, Fragment} from 'react';
import './Modal.component.scss';

export class ModalComponent extends Component {

  constructor(props) {
    super(props);

    this.cancelButtonRef = createRef();
  }

  render() {
    return (
      <Transition.Root show={this.props.open} as={Fragment}>
        <Dialog
          as="div"
          static
          className="modal-component"
          initialFocus={this.cancelButtonRef}
          open={this.props.open}
          onClose={this.props.onClose}
        >
          <div className="dialog-content">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="dialog-overlay" />
            </Transition.Child>

            <span className="dialog-center" aria-hidden="true">
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div
                className="dialog-body"
                ref={this.cancelButtonRef}
              >
                <div
                  className="children-wrapper">
                  {this.props.children}
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
    );
  }
}

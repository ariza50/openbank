const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors:
      {
        primary: '#002B45',
        secondary: '#FF0049',
        tertiary: '#FFF1E5',
        gray: colors.coolGray,
        white: colors.white,
        black: colors.black,
        blue: colors.blue,
        indigo: colors.indigo,
        red: colors.red,
        transparent: 'transparent',
        current: 'currentColor',
      },
    extend: {},
  },
  variants: {
    extend: {
      opacity: ['disabled'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('tailwindcss-font-inter')(),
  ],
}
